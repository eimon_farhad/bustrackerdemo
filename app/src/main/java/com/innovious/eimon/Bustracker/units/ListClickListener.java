package com.innovious.eimon.Bustracker.units;

public interface ListClickListener {
    void onBoardClick(String childId,String token,String clas);
    void onDeboardClick(String childId,String token);
}
