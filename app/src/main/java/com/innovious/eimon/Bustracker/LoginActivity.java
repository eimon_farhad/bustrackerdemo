package com.innovious.eimon.Bustracker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.innovious.eimon.Bustracker.BUS.BusDashBoardActivity;
import com.innovious.eimon.Bustracker.Service.LocationMonitoringService;
import com.innovious.eimon.Bustracker.units.FireBaseToken;
import com.innovious.eimon.Bustracker.units.HideKeyBoard;
import com.innovious.eimon.Bustracker.units.UserPref;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity{
RelativeLayout btn_login;
EditText edt_phno;
String phone_number;
String  otp;
int RC_SIGN_IN=5;
    PinEntryEditText entryEditText;
    BottomSheetDialog bottomSheetDialog;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_main);
        btn_login=findViewById(R.id.login_btn);
        edt_phno=findViewById(R.id.edt_phno);
 progressDialog=new ProgressDialog(this);
 progressDialog.setMessage("Loading.....");
 progressDialog.setCanceledOnTouchOutside(false);
        btn_login.setOnClickListener(v->{
            phone_number="+91"+edt_phno.getText().toString();
            if(FirebaseAuth.getInstance().getCurrentUser()==null){
                generateotp(phone_number);
            }else{
                callintent();
            }
        });
    }


    private void createbottumsheetDialog(){
        bottomSheetDialog=new BottomSheetDialog(this);
        bottomSheetDialog.setContentView(R.layout.bottam_otp);
        TextView textView=bottomSheetDialog.findViewById(R.id.txt_phone_number);
        TextView editText=bottomSheetDialog.findViewById(R.id.txt_phno);
        editText.setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
        });
        entryEditText=bottomSheetDialog.findViewById(R.id.edt_otp);
        SpannableStringBuilder stringBuilder=new SpannableStringBuilder();
        SpannableString string1=new SpannableString("We Sent otp to ");
        string1.setSpan(new ForegroundColorSpan(Color.BLACK),0,string1.length(),0);
        stringBuilder.append(string1);
        SpannableString string2=new SpannableString(edt_phno.getText().toString());
        string2.setSpan(new ForegroundColorSpan(Color.RED),0,string2.length(),0);
        stringBuilder.append(string2);
        SpannableString string3=new SpannableString(" .Click Edit to change phone number");
        string3.setSpan(new ForegroundColorSpan(Color.BLACK),0,string3.length(),0);
        stringBuilder.append(string3);
        textView.setText(stringBuilder,TextView.BufferType.SPANNABLE);

        editText.setOnClickListener(v-> bottomSheetDialog.dismiss());
        entryEditText.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable){
                if(editable.length()=="123456".length()){
                    verifyotp(editable.toString());
                }
            }
        });
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();
    }

    private void generateotp(String phone){
        progressDialog.show();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(phone, 30L, TimeUnit.SECONDS, LoginActivity.this, new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential){
progressDialog.dismiss();
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
                        Toast.makeText(LoginActivity.this," Incorrect Phone Number" ,Toast.LENGTH_LONG ).show();

                    }

                    @Override
                    public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        super.onCodeSent(s, forceResendingToken);
                        otp=s;
                        btn_login.setEnabled(true);
                        progressDialog.dismiss();
                        createbottumsheetDialog();
                        Toast.makeText(LoginActivity.this,"Code Sent" ,Toast.LENGTH_SHORT ).show();
                    }
                }
        );
    }
    private void verifyotp(String fire_otp){
        progressDialog.show();
        HideKeyBoard.hide(edt_phno);
        PhoneAuthCredential credential=PhoneAuthProvider.getCredential(otp,fire_otp);
        FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener(task -> {
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if(task.isSuccessful()){
                FireBaseToken.getInstance().saveFirebaseToken(this);

                Toast.makeText(LoginActivity.this,"Otp Verifed" ,Toast.LENGTH_SHORT ).show();
                edt_phno.setText("");
                bottomSheetDialog.dismiss();
                progressDialog.dismiss();
            //    saveParentsDatatoFireBase();

            }else {
                Toast.makeText(LoginActivity.this,"Otp not matching" ,Toast.LENGTH_SHORT ).show();
                progressDialog.dismiss();

            }
        });
    }

    private void callintent(){
        Intent intent=new Intent(LoginActivity.this, LocationMonitoringService.class);
        stopService(intent);
        if(edt_phno.getText().toString().equalsIgnoreCase("1")){
            startActivity(new Intent(LoginActivity.this,ParentDashBoardActivity.class));
        }else if(edt_phno.getText().toString().equalsIgnoreCase("2")) {
            startActivity(new Intent(LoginActivity.this, BusDashBoardActivity.class));
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
//        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
//            finish();
//            startActivity(new Intent(this, ParentDashBoardActivity.class));
//        }
   }


}
