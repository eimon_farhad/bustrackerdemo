package com.innovious.eimon.Bustracker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.innovious.eimon.Bustracker.Fragments.WardsListFragment;

public class FragmentContainerActivity extends AppCompatActivity {
String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        savedInstanceState=getIntent().getExtras();
        if(savedInstanceState!=null){
            type=savedInstanceState.getString("type");
        }
        if(type.equalsIgnoreCase("addwards")){
            replacefragment(new WardsListFragment());
        }

    }
    public  void replacefragment(Fragment fg){
        String backStateName = fg.getClass().getName();
      //  boolean fragmentPopped = getSupportFragmentManager().popBackStackImmediate(backStateName, 0);

     //   if (!fragmentPopped) {
            FragmentManager fm=getSupportFragmentManager();
            FragmentTransaction ft=fm.beginTransaction();


            ft.replace(R.id.fragment_container,fg).commit();


       // }

    }


}
