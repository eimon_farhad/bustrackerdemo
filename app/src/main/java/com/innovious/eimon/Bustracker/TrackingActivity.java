package com.innovious.eimon.Bustracker;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class TrackingActivity extends AppCompatActivity implements OnMapReadyCallback {
    private SupportMapFragment mapFragment;
    private Socket socket;
    GoogleMap myMap;
    Marker marker;
    private boolean isMarkerRotating = false;
    public Polyline cyanPolyline;
    public String socket_name = "user", socket_room = "21";
    private List<LatLng> locationHistoryList;
    private JSONObject data;
    private double lat = 0.0, lng = 0.0;
    private LocationManager locMan;
    public Location mylocation;
    LatLng myLatLng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_map_view);
        mapFragment.getMapAsync(this);
        locationHistoryList = new ArrayList<>();


        //Socket Connection
        MyApplication app = (MyApplication) getApplication();
        socket = app.getSocket();
        socket.on(Socket.EVENT_CONNECT, onConnect);

        socket.on("getlatlong", getlatlong);

        socket.connect();


//        if (ManagePermission.checkPermission(this)) {
//
//
//            if (gpsTracker.canGetLocation() == false) {
//
//                gpsTracker.showSettingsAlert();
//
//            } else {
//                locMan = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//                Criteria criteria = new Criteria();
//                criteria.setPowerRequirement(Criteria.POWER_HIGH);
//                criteria.setAccuracy(Criteria.ACCURACY_FINE);
//                criteria.setAltitudeRequired(false);
//                criteria.setBearingRequired(true);
//                criteria.setCostAllowed(true);
//                criteria.setSpeedRequired(true);
//                String bestProvider = locMan.getBestProvider(criteria, true);
//                locMan.requestLocationUpdates(bestProvider, 5000, 5, this);
//                locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 2, this);
//                locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 2000, 2, this);
//                mylocation = locMan.getLastKnownLocation(bestProvider);
//
//                if (mylocation != null) {
//                    myLatLng = new LatLng(mylocation.getLatitude(), mylocation.getLongitude());
//                }
//            }
//        } else {
//            ManagePermission.requestPermission(this);
//        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener(){
        @Override
        public void call(Object... args) {

            try {
                JSONObject obj = new JSONObject();

                /*obj.put("name", "cartrack");
                obj.put("room", "9474094290");*/

                obj.put("name", socket_name);
                obj.put("room", socket_room);

                socket.emit("join", obj);

                Log.i("joined", "socket connected");

            } catch (JSONException je) {
                je.printStackTrace();
            }

        }
    };
    //Getting Response from Socket
    private Emitter.Listener getlatlong = new Emitter.Listener(){
        @Override
        public void call(final Object... args) {
            TrackingActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        data = (JSONObject) args[0];

                        if (data != null) {

                            JSONObject message = data.getJSONObject("message");

                            lat = Double.parseDouble(message.getString("lat"));

                            Log.i("slat", ":: " + lat);

                            lng = Double.parseDouble(message.getString("lng"));

                            Log.i("slong", ":: " + lng);

                            locationHistoryList.add(new LatLng(lat, lng));

                            Log.i("location_size", "[" + locationHistoryList.size() + "]");

                        }

                    } catch (JSONException e) {
                     Log.i("Tracking",""+e);
                        return;
                    }
                    try {
                        if (locationHistoryList.size() > 1) {
                            marker.setVisible(true);
                            Log.i("start_end", "::" + locationHistoryList.get(locationHistoryList.size() - 2) + ">>" + locationHistoryList.get(locationHistoryList.size() - 1));

                          //  boolean hasAccuracy = mylocation.hasAccuracy();

                      //      Log.i("hasAccuracy", ">>" + hasAccuracy);

                          //  if (hasAccuracy) {

                                PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
                                for (int z = 0; z < locationHistoryList.size() - 1; z++) {
                                    LatLng point = locationHistoryList.get(z);
                                    options.add(point);
                     //           }
                                cyanPolyline = myMap.addPolyline(options);
                            }


                            animateMarker(marker, locationHistoryList.get(locationHistoryList.size() - 1), false);
                            //marker.setPosition(locationHistoryList.get(locationHistoryList.size() - 1));
                            double bearing = getBearing(locationHistoryList.get(locationHistoryList.size() - 2), locationHistoryList.get(locationHistoryList.size() - 1));
                            Log.i("bearing", "::" + bearing);

                            if (bearing > 20) {
                                //marker.setRotation((float) bearing);
                                rotateMarker(marker, (float) bearing);
                            }
                            marker.setAnchor(0.5f, 0.5f);
                            marker.setFlat(false);






                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(locationHistoryList.get(locationHistoryList.size() - 1))   // Sets the center of the map to Mountain View
                                    .zoom(16.5f )                   // Sets the zoom// Sets the orientation of the camera to east// Sets the tilt of the camera to 30 degrees
                                    .build();// Creates a CameraPosition from the builder
                            myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            myMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));



//                            myMap.moveCamera(CameraUpdateFactory
//                                    .newCameraPosition
//                                            (new CameraPosition.Builder()
//                                                    .target(locationHistoryList.get(locationHistoryList.size() - 1))
//                                                    .zoom(15.5f)
//                                                    .build()));

                        }
                    } catch (Exception e) {
                        Log.i("TrackingActivity", "" + e);
                    }
                }
            });
        }
    };


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            myMap = googleMap;
        }
        if (ManagePermission.checkPermission(TrackingActivity.this)) {
            setuplocation(googleMap);
            setupMap();

           // myMap.animateCamera(CameraUpdateFactory.zoomTo(newZoom), duration);

            myMap.animateCamera(CameraUpdateFactory.zoomIn());
            myMap.animateCamera(CameraUpdateFactory.zoomTo(16.5f), 2000, null);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(myLatLng)      // Sets the center of the map to Mountain View
                    .zoom(16.5f)                   // Sets the zoom// Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            myMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                    .target(googleMap.getCameraPosition().target)
//                    .zoom(13f)
//                    .bearing(30)
//                    .tilt(45)
//                    .build()));


        } else {
            ManagePermission.requestPermission(TrackingActivity.this);
        }


    }



    private void setuplocation(GoogleMap myMap) {
        if (ManagePermission.checkPermission(this)) {
            GPSTracker gpsTracker = new GPSTracker(this);


            if (gpsTracker.canGetLocation() == false) {

                gpsTracker.showSettingsAlert();

            } else {
                gpsTracker.getLatitude();




                    myLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                    marker = myMap.addMarker(new MarkerOptions()
                            .position(myLatLng)
                            .flat(false)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_bus)));
                    marker.setVisible(false);

                // myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 14));

            }
        } else {
            ManagePermission.requestPermission(this);
        }
    }

    private void setupMap() {

        if (myMap == null) {

            mapFragment.getMapAsync(this);
        } else {
            myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            // Enable MyLocation Button in the Map
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            myMap.setMyLocationEnabled(true);

            myMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Zoom level 15
            myMap.animateCamera(CameraUpdateFactory.zoomTo(15f));

            // Enable / Disable zooming controls
            myMap.getUiSettings().setZoomControlsEnabled(true);

            // Enable / Disable my location button
            myMap.getUiSettings().setMyLocationButtonEnabled(true);

            // Enable / Disable Compass icon
            myMap.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            myMap.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable zooming functionality
            myMap.getUiSettings().setZoomGesturesEnabled(true);

            myMap.setTrafficEnabled(false);

            myMap.setIndoorEnabled(false);

            myMap.setBuildingsEnabled(true);

            myMap.getUiSettings().setZoomControlsEnabled(true);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        socket.close();
        socket.off("");
    }

    private float getBearing(LatLng begin, LatLng end) {

        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    //MARKAR ANIMATION
    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = myMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                //marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_top));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }



}
