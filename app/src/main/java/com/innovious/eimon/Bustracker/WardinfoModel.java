package com.innovious.eimon.Bustracker;

public class WardinfoModel {
    String name;
    String age;
    String gender;
    String clas;
    String image,G_name,G_phno;
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token){
        Token = token;
    }

    String Token;


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getG_name() {
        return G_name;
    }

    public void setG_name(String g_name) {
        G_name = g_name;
    }

    public String getG_phno() {
        return G_phno;
    }

    public void setG_phno(String g_phno) {
        G_phno = g_phno;
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getClas() {
        return clas;
    }

    public void setClas(String clas) {
        this.clas = clas;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
