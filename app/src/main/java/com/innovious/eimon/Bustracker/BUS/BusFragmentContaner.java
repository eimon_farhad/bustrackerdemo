package com.innovious.eimon.Bustracker.BUS;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.innovious.eimon.Bustracker.BUS.Fragment.AddPickUpPointFragment;
import com.innovious.eimon.Bustracker.BUS.Fragment.DropFragment;
import com.innovious.eimon.Bustracker.BUS.Fragment.PickupFragment;
import com.innovious.eimon.Bustracker.R;

public class BusFragmentContaner extends AppCompatActivity{

String type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_pick_up);
        savedInstanceState=getIntent().getExtras();
        if(savedInstanceState!=null){
            type=savedInstanceState.getString("pickuptype");
        }
if(type.equalsIgnoreCase("pickup")){
    replacefragment(new AddPickUpPointFragment(),"");
}else if(type.equalsIgnoreCase("pickfromhome")){
    replacefragment(new PickupFragment(),type);
}else if(type.equalsIgnoreCase("droptohome")){
    replacefragment(new PickupFragment(),type);
}
    }

    private void replacefragment(Fragment fg,String type) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Bundle bundle=new Bundle();
        bundle.putString("pickuptype",type);
        fg.setArguments(bundle);
            ft.replace(R.id.bus_fragment_container, fg).commit();
        }

}
