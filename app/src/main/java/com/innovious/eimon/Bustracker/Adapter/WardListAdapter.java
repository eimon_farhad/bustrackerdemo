package com.innovious.eimon.Bustracker.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.firestore.DocumentReference;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.WardinfoModel;
import com.innovious.eimon.Bustracker.units.ListClickListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class WardListAdapter extends RecyclerView.Adapter<WardListAdapter.ViewHolder> {
    Context context;
    ArrayList<WardinfoModel>arrayList;
    ListClickListener listener;
    String pickuptype;

    public WardListAdapter(Context context, ArrayList<WardinfoModel> arrayList,String pickuptype) {
        this.context = context;
        this.arrayList = arrayList;
        this.pickuptype=pickuptype;
    }

    @NonNull
    @Override
    public WardListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
View view= LayoutInflater.from(context).inflate(R.layout.listitem,viewGroup,false);
return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WardListAdapter.ViewHolder viewHolder, int i) {
  viewHolder.name.setText(arrayList.get(i).getName());
  viewHolder.age.setText("Age- "+arrayList.get(i).getAge());
  viewHolder.clas.setText("Class- "+arrayList.get(i).getClas());
  viewHolder.gender.setText("Gender- "+arrayList.get(i).getGender());
   loadimage(arrayList.get(i).getImage(),viewHolder.img);
if(pickuptype.equalsIgnoreCase("droptohome")){
    viewHolder.on_board.setVisibility(View.GONE);
    viewHolder.de_board.setVisibility(View.VISIBLE);
}
viewHolder.de_board.setOnClickListener(v->{
    viewHolder.de_board.setBackgroundColor(Color.GRAY);
    viewHolder.de_board.setEnabled(false);
    listener.onDeboardClick(arrayList.get(i).getId(),arrayList.get(i).getToken());
    notifyDataSetChanged();
});
viewHolder.on_board.setOnClickListener(v -> {
    viewHolder.on_board.setBackgroundColor(Color.GRAY);
    viewHolder.on_board.setEnabled(false);
listener.onBoardClick(arrayList.get(i).getId(),arrayList.get(i).getToken(),arrayList.get(i).getClas());
notifyDataSetChanged();

});

    }

    private void loadimage(String image,ImageView view) {
        Glide.with(context)
                .load(image).centerCrop()
                .into(view);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView name,age,gender,clas;
        Button on_board,de_board;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            on_board=itemView.findViewById(R.id.on_board);
            de_board=itemView.findViewById(R.id.de_board);
            img=itemView.findViewById(R.id.img);
            name=itemView.findViewById(R.id.name);
            gender=itemView.findViewById(R.id.gender);
            clas=itemView.findViewById(R.id.clas);
            age=itemView.findViewById(R.id.age);


        }
    }
    public void OnBoardClick(ListClickListener listener){
   this.listener=listener;
    }
}
