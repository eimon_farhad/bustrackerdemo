package com.innovious.eimon.Bustracker;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.innovious.eimon.Bustracker.units.UserPref;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ParentDashBoardActivity extends AppCompatActivity {
Button add_wards,tracking_,home_to_school,school_to_home;
ImageView menu_item;
ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_dash_board);
        add_wards=findViewById(R.id.add_wards);
        tracking_=findViewById(R.id.tracking_);
        menu_item=findViewById(R.id.menu_item);
        home_to_school=findViewById(R.id.home_to_school);
        school_to_home=findViewById(R.id.school_to_home);
FirebaseMessaging.getInstance().subscribeToTopic("SS");

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
       // progressDialog.show();



        home_to_school.setOnClickListener(v->{
            createdialog();
        });


        add_wards.setOnClickListener(v -> {
            Intent intent=new Intent(ParentDashBoardActivity.this,FragmentContainerActivity.class);
            intent.putExtra("type","addwards");
            startActivity(intent);
        });
        tracking_.setOnClickListener(v -> {
            Intent intent=new Intent(ParentDashBoardActivity.this,TrackingActivity.class);
            intent.putExtra("type","track");
            startActivity(intent);
        });

        menu_item.setOnClickListener(v->{
            PopupMenu popup = new PopupMenu(ParentDashBoardActivity.this, v);
           // popup.setOnMenuItemClickListener(MainActivity.this);
            popup.inflate(R.menu.menuoptions);
            popup.show();
        });    }

    private void createdialog() {

        Calendar calendar=Calendar.getInstance();
        SimpleDateFormat dateFormat=new SimpleDateFormat("EEE, d MMM yyyy");
        AlertDialog.Builder alertbuilder=new AlertDialog.Builder(this);
        alertbuilder.setTitle(dateFormat.format(calendar.getTime()));
        alertbuilder.setMessage("Estimate time to pick up is 8:00 AM");
        alertbuilder.setPositiveButton("Ok", (dialog, which) -> dialog.dismiss());
        AlertDialog dialog=alertbuilder.create();
        dialog.show();
    }


}
