package com.innovious.eimon.Bustracker.BUS;

import android.app.Dialog;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.innovious.eimon.Bustracker.BUS.Fragment.ReportActivty;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.Service.LocationMonitoringService;
import com.innovious.eimon.Bustracker.units.SendMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class BusDashBoardActivity extends AppCompatActivity implements SendMessage.MessageLisner {
RelativeLayout pick_up,drop;
Button add_pickpoint,report;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_choose_service);
        pick_up=findViewById(R.id.pick_up);
        drop=findViewById(R.id.drop);
        report=findViewById(R.id.report);
        add_pickpoint=findViewById(R.id.add_pickpoint);

        drop.setOnClickListener(v->{
            //createdialog();
            Intent intent=new Intent(this, BusFragmentContaner.class);
            intent.putExtra("pickuptype","droptohome");
            startActivity(intent);

        });
        report.setOnClickListener(v->{
   startActivity(new Intent(this, ReportActivty.class));
        });
        pick_up.setOnClickListener(v -> {
            Intent intent=new Intent(this, BusFragmentContaner.class);
            intent.putExtra("pickuptype","pickfromhome");
            startActivity(intent);
            //createdialog();
            //Sendmsg();
        });
        add_pickpoint.setOnClickListener(v->{
            Intent intent=new Intent(this, BusFragmentContaner.class);
            intent.putExtra("pickuptype","pickup");
            startActivity(intent);

        });

    }

//    private void Sendmsg(){
//String url="https://fcm.googleapis.com/fcm/notification";
//        String array[]={"csCt_DOonPc:APA91bGVjNS_Zz19iUIc9PJI1iZdVkKlFKmHko_IR5ohquqcYnQ3tV7oxGlbG4fLw-dRN9oh-91QQI1XwVKCQ0iMq-5dePU3EfCVl3KWwkhBuz2GWDy15DUYbX8nvREd3T5b13C6UWkB"
//                ,"fsV-3iEat74:APA91bGNzc0VlLhQxKlTTrvA7A4RFzY2m--HwKJSu4LfFJaselMj4ed9BC3xjZ8sxuL3ZyMtCBIR1Cv8iMU5lOdYboD7eBpVFbAqru7HjtAnvQrA04T-4mpNBWF5NMU4nglTipN6_7Bk"};
//        JSONObject object=new JSONObject();
//
//try {
//    object.put("operation","create");
//    object.put("notification_key_name","mygroup");
//    object.put("registration_ids",array);
//
//}catch (Exception e){
//    Log.i("gg","dd");
//}
//new SendMessage("gg",object,url,this, Request.Method.POST,this).makehttprequest();
//
//
//    }

    private void createdialog(){
        Dialog dialog=new Dialog(BusDashBoardActivity.this);
        dialog.setContentView(R.layout.dialog_);
        TextView textView=dialog.findViewById(R.id.start_journy);
        textView.setOnClickListener(onclick->{
ContextCompat.startForegroundService(BusDashBoardActivity.this,new Intent(BusDashBoardActivity.this, LocationMonitoringService.class));
startActivity(new Intent(this, BusFragmentContaner.class));
dialog.dismiss();
        });

        dialog.show();
    }


    @Override
    public String onResponse(String response, String type) {
        try {
            JSONObject jsonObject=new JSONObject(response);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public VolleyError onFailure(VolleyError error) {
        return null;
    }
}
