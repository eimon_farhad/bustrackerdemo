package com.innovious.eimon.Bustracker.units;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPref {

SharedPreferences sharedPreferences;
SharedPreferences.Editor editor;
Context context;

    public UserPref(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences("user_pref_file", Context.MODE_PRIVATE);
        editor =  sharedPreferences.edit();
    }
    public void saveFirebasetoken(String token){
editor.putString("token",token).commit();
    }
    public String getToken(){
        return sharedPreferences.getString("token","");
    }

}
