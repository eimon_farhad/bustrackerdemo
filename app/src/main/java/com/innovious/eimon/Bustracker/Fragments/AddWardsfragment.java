package com.innovious.eimon.Bustracker.Fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;

import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.innovious.eimon.Bustracker.BUS.Fragment.AddPickUpPointFragment;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.units.UserPref;
import com.theartofdev.edmodo.cropper.CropImage;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class AddWardsfragment extends Fragment implements View.OnClickListener {
    CardView layout_img;
    TextView tt,add_pickup_point;
    ImageView image,_img_back;
String address="";
Button btn_submit;
boolean isval=true;
RadioGroup radio_group;
RadioButton radioButton;
String img_url="";
     ProgressDialog progressDialog;
     double picklat,picklng;

TextInputLayout text_input_layout_name,text_input_layout_age,text_input_layout_class,text_input_layout_g_name,text_input_layout_g_phno;
EditText editText_name,editText_age,editText_clas,editText_gurd_name,editText_gurd_phno;
int radio_button_id=0;
FirebaseAuth auth;
    FirebaseUser user;
    String ph_no;
    FirebaseFirestore firestore;
    RadioButton male,female;
    String id;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
return inflater.inflate(R.layout.addwardsfragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        initview(view);
        initfireStore();
        savedInstanceState=getArguments();
        if(savedInstanceState!=null) {
            address = savedInstanceState.getString("address");
            picklat=savedInstanceState.getDouble("lat");
            picklng=savedInstanceState.getDouble("lng");
            add_pickup_point.setText(address);
        }
    }

    private void initfireStore(){

        auth=FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        ph_no = auth.getCurrentUser().getPhoneNumber();
        firestore= FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);
        progressDialog.show();
        firestore.collection("Parents")
                .whereEqualTo("Phone_number",ph_no)
                .get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
for(QueryDocumentSnapshot snapshot:task.getResult()){
id=snapshot.getId();
}
                progressDialog.dismiss();
            }
        }).addOnFailureListener(e -> {
  progressDialog.dismiss();
  Toast.makeText(getContext(),""+e,Toast.LENGTH_LONG).show();
        });
    }

    private void initview(View view){

        editText_name=view.findViewById(R.id.editText_name);
        editText_age=view.findViewById(R.id.editText_age);
        editText_clas=view.findViewById(R.id.editText_clas);
        editText_gurd_name=view.findViewById(R.id.editText_gurd_name);
        editText_gurd_phno=view.findViewById(R.id.editText_gurd_phno);
        editText_gurd_phno.setText(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber());
        editText_gurd_phno.setEnabled(false);

        text_input_layout_name=view.findViewById(R.id.text_input_layout_name);
        text_input_layout_age=view.findViewById(R.id.text_input_layout_age);
        text_input_layout_class=view.findViewById(R.id.text_input_layout_class);
        text_input_layout_g_name=view.findViewById(R.id.text_input_layout_g_name);
        text_input_layout_g_phno=view.findViewById(R.id.text_input_layout_g_phno);

        radio_group=view.findViewById(R.id.radio_group);
        male=view.findViewById(R.id.male);
        female=view.findViewById(R.id.female);


        layout_img=view.findViewById(R.id.layout_img);
        tt=view.findViewById(R.id.tt);
        _img_back=view.findViewById(R.id._img_back);
        image=view.findViewById(R.id.image);
        add_pickup_point=view.findViewById(R.id.add_pickup_point);
        btn_submit=view.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(this);
        layout_img.setOnClickListener(this);
        _img_back.setOnClickListener(this);
        add_pickup_point.setOnClickListener(this);
        progressDialog=new ProgressDialog(getContext());
        male.setChecked(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_img:
                try {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, 1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id._img_back:
                replacefragment(new WardsListFragment());
                break;
            case R.id.add_pickup_point:
               replacefragment(new ParentAddPickUpPointFragment());
                break;
            case R.id.btn_submit:
                if(formvalidation()) {
                  //  replacefragment(new WardsListFragment());
                    savedatatofirebase();
                }
                break;
        }
    }

    private void savedatatofirebase() {
    progressDialog.show();
        Map<String,String>map=new HashMap<>();
        map.put("Ward's_Name",editText_name.getText().toString().trim());
        map.put("Age",editText_age.getText().toString().trim());
        map.put("clas",editText_clas.getText().toString().trim());
        map.put("gender",gender);
        map.put("Guardian's_Name",editText_gurd_name.getText().toString().trim());
        map.put("Guardian_phno",editText_gurd_phno.getText().toString().trim());
        map.put("img_url",img_url);
        map.put("picklat", String.valueOf(picklat));
        map.put("picklng",String.valueOf(picklng));
        map.put("pickaddress",address);
        map.put("parents_id",id);
        map.put("parents_token",new UserPref(getContext()).getToken());




 //   .document(uid).collection("wards");
   // firestore.collection("Parents").document(uid);
CollectionReference reference=firestore.collection("Son");
         //       .document(uid).collection("wards");

        reference.add(map)
                .addOnSuccessListener(Reference -> {
            progressDialog.dismiss();
            Toast.makeText(getContext(), "saved", Toast.LENGTH_LONG).show();

            replacefragment(new WardsListFragment());
                })
                .addOnFailureListener(e -> {
            progressDialog.dismiss();
            Toast.makeText(getContext(), ""+e, Toast.LENGTH_LONG).show();
        });
    }

    public void replacefragment(Fragment fg) {
        String backStateName = fg.getClass().getName();
        boolean fragmentPopped = getChildFragmentManager().popBackStackImmediate(backStateName, 0);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (!fragmentPopped) {
            ft.addToBackStack(null);
            ft.replace(R.id.fragment_container, fg).commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1) {
            if (data != null) {
                Uri contentURI = data.getData();
//uploadfile(contentURI);
                try {
    CropImage.activity(contentURI)
            .setAspectRatio(1, 1)
            .setFixAspectRatio(true)
            .start(getContext(), this);
}catch (Exception e){
    Log.d("mytag",""+e);
}

            }
        }

        if(requestCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            //String TAG = MainActivity.class.getSimpleName();

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                //  img.setImageURI(result.getUri());
                try {
                    String path=result.getUri().getPath();

                    Bitmap bitmap= MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), result.getUri());
                    //  img.setImageBitmap(bitmap);

                    try {
                        uploadBitmap(path,bitmap);

                        //  byte[] bytes=  ImageUtils.getInstant().getBitmapBytes(bitmap);
                    }catch (Exception e){
                   Log.i("TAg",""+e);
                    }
                    tt.setVisibility(View.GONE);
                    image.setVisibility(View.GONE);
                    layout_img.setBackground(new BitmapDrawable(getActivity().getResources(),bitmap));


                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                //  Log.d(TAG, "onActivityResult: " + error.getMessage());
                Toast.makeText(getContext(),""+error,Toast.LENGTH_SHORT).show();
            }
        }
    }

//
//    private void uploadfile(Uri contentURI) {
//
//        FirebaseStorage storage = FirebaseStorage.getInstance();
//        StorageReference storageReference = storage.getReference();
//        StorageReference ref = storageReference.child("images/"+ UUID.randomUUID().toString());
//        ref.putFile(contentURI);
//    }

    //upload to Storage
    private void uploadBitmap(String path,Bitmap bitmap) {
        progressDialog.setTitle("Uploading...");
        progressDialog.show();
        Uri file = Uri.fromFile(new File(path));
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference imagesRef = storage.getReferenceFromUrl("gs://bustrackerdemo-8b863.appspot.com");
        StorageReference myfileRef = imagesRef.child("images/"+file.getLastPathSegment());

        UploadTask uploadTask = myfileRef.putFile(file);


uploadTask.addOnProgressListener(taskSnapshot -> {
    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
            .getTotalByteCount());
    progressDialog.setMessage("Uploaded "+(int)progress+"%");
});
   uploadTask.continueWithTask(task -> {
       if (!task.isSuccessful()) {
           progressDialog.dismiss();
           throw task.getException();
       }
       // Continue with the task to get the download URL
       return myfileRef.getDownloadUrl();
   }).addOnCompleteListener(task -> {
       if (task.isSuccessful()) {
           Uri downloadUri = task.getResult();
           progressDialog.dismiss();
           img_url=""+downloadUri;
       } else {
         Toast.makeText(getContext(),"Could Not Upload Image,Try Again",Toast.LENGTH_SHORT).show();
       }
   });

//uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//    @Override
//    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
//        progressDialog.dismiss();
//        Uri downloadUrl =taskSnapshot.getUploadSessionUri();
//        String DOWNLOAD_URL = downloadUrl.getPath();
//        Log.i("Log",DOWNLOAD_URL);
//
//
//        Toast.makeText(getContext(), "Uploaded", Toast.LENGTH_SHORT).show();
//
//    }
//}).addOnFailureListener(new OnFailureListener() {
//    @Override
//    public void onFailure(@NonNull Exception e) {
//        progressDialog.dismiss();
//        Toast.makeText(getContext(), "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
//
//    }
//}).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//    @Override
//    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//        double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
//                .getTotalByteCount());
//        progressDialog.setMessage("Uploaded "+(int)progress+"%");
//    }
//});




    }

    String gender;
    private boolean formvalidation(){
        radio_button_id=radio_group.getCheckedRadioButtonId();
        radioButton=getActivity().findViewById(radio_button_id);
gender= (String) radioButton.getText();

        if(editText_name.getText().toString().isEmpty()){
    text_input_layout_name.setErrorEnabled(true);
    text_input_layout_name.setError("Field empty");
    editText_name.requestFocus();
    isval=false;
    return isval ;
}else if(editText_age.getText().toString().isEmpty()){
    text_input_layout_age.setErrorEnabled(true);
    text_input_layout_age.setError("Field empty");
    editText_age.requestFocus();
    isval=false;
    return isval ;
}else if (editText_clas.getText().toString().isEmpty()){
    text_input_layout_class.setErrorEnabled(true);
    text_input_layout_class.setError("Field empty");
    editText_clas.requestFocus();
    isval=false;
    return isval ;
}else if(editText_gurd_name.getText().toString().isEmpty()){
    text_input_layout_g_name.setErrorEnabled(true);
    text_input_layout_g_name.setError("Field empty");
    editText_gurd_name.requestFocus();
    isval=false;
    return isval ;
}else if(editText_gurd_phno.getText().toString().isEmpty()){
    text_input_layout_g_phno.setErrorEnabled(true);
    text_input_layout_g_phno.setError("Field empty");
    editText_gurd_phno.requestFocus();
    isval=false;
    return isval ;
}else if(radio_button_id==-1){
Toast.makeText(getContext(),"Please Select Gender",Toast.LENGTH_SHORT).show();
isval=false;
return isval;
}else if(img_url.equalsIgnoreCase("")){
            Toast.makeText(getContext(),"Please Add Image",Toast.LENGTH_SHORT).show();
            isval=false;
            return isval;
        }else if(address.equalsIgnoreCase("")){
            Toast.makeText(getContext(),"please add pickup point",Toast.LENGTH_SHORT).show();
            isval=false;
            return isval;
        }
        return true;
    }
}
