package com.innovious.eimon.Bustracker.BUS.Fragment;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.messaging.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.messaging.FirebaseMessaging;
import com.innovious.eimon.Bustracker.Adapter.WardListAdapter;
import com.innovious.eimon.Bustracker.GPSTracker;
import com.innovious.eimon.Bustracker.LocationPermission;
import com.innovious.eimon.Bustracker.ManagePermission;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.Service.GetLocationService;
import com.innovious.eimon.Bustracker.WardinfoModel;
import com.innovious.eimon.Bustracker.units.ListClickListener;
import com.innovious.eimon.Bustracker.units.SendMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static android.content.Context.MODE_PRIVATE;
import static com.google.android.gms.maps.model.JointType.ROUND;
import static com.innovious.eimon.Bustracker.Service.FireBaseMessaging.Intent_Action;
import static java.security.AccessController.getContext;

public class NextPickUpMapFragment extends Fragment implements OnCompleteListener<QuerySnapshot>,
        OnMapReadyCallback, OnSuccessListener<Location> , LocationListener ,GoogleApiClient.ConnectionCallbacks
,  GoogleApiClient.OnConnectionFailedListener, SendMessage.MessageLisner, ListClickListener {
    LinearLayout bottom_sheet;
    public static final String TAG = NextPickUpMapFragment.class.getSimpleName();
    BroadcastReceiver georeceiver,msgreceiver;
    String child_id,Parents_token,pickuptype;

    RecyclerView recycler_list;
    WardListAdapter wardListAdapter;
    ArrayList<WardinfoModel> arrayList;
    String Address;
    FirebaseFirestore firestore;
    ProgressDialog progressDialog;
    private SupportMapFragment mapFrag;
    GoogleApiClient apiClient;
    public Location mylocation;
    private LocationManager locMan;
    LatLng myLatLng;
    Marker marker;
    double picklat,picklng;
    List<LatLng>polyLineList;
    Button start_journy;

    GoogleMap googleMap;
    LocationPermission locationPermission;
    private int LOCATION_PERMISSION_CONSTANT = 200;
    private static final int REQUEST_PERMISSION_SETTING = 301;

    ArrayList<LatLng> location_list;
ImageView arrow_left,arrow_right;
String count;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.next_pick_up_map_frag, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        savedInstanceState = getArguments();
        if (savedInstanceState != null) {
            Address = savedInstanceState.getString("address");
            picklat=savedInstanceState.getDouble("lat");
            picklng=savedInstanceState.getDouble("lng");
            count=savedInstanceState.getString("count");
            pickuptype=savedInstanceState.getString("pickuptype");        }
        locationPermission=new LocationPermission(getContext(),getActivity(),this);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        progressDialog.setCanceledOnTouchOutside(false);
       location_list =new ArrayList<>();
        polyLineList=new ArrayList<>();
        mapFrag = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.fragment_map_view);

        if (ManagePermission.checkPermission(getActivity())) {


            bottom_sheet = view.findViewById(R.id.bottom_sheet);
            recycler_list = view.findViewById(R.id.recycler_list);
            arrow_left=view.findViewById(R.id.arrow_left);
            arrow_right=view.findViewById(R.id.arrow_right);
            start_journy=view.findViewById(R.id.start_journy);
            arrayList = new ArrayList<>();
            firestore = FirebaseFirestore.getInstance();
            firestore.collection("Son").whereEqualTo("pickaddress",Address)
                    .get()
                    .addOnCompleteListener(this).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.i("TAG", "" + e);
                }
            });

            start_journy.setOnClickListener(v->{
                navigatetolatlng(Address);


            });
            ArrayList<Integer>list=new ArrayList<>();

            msgreceiver=new BroadcastReceiver(){
                @Override
                public void onReceive(Context context, Intent intent) {
                    int index=0;
                    child_id=intent.getStringExtra("child_id");
                    Parents_token=intent.getStringExtra("usertoken");
                    for(int i=0;i<arrayList.size();i++){
                        if(child_id==arrayList.get(i).getId()){
                      list.add(i);
                            break;
                        }
                    }
                }
            };

            georeceiver=new BroadcastReceiver(){
    @Override
    public void onReceive(Context context, Intent intent) {
  String s=intent.getStringExtra("Status");
 if(s.equalsIgnoreCase("Entering"));
 progressDialog.show();
 for(int i=0;i<arrayList.size();i++){
sendmessage(arrayList.get(i).getToken(),s);
 }
 progressDialog.dismiss();
    }
};

            initializeBottomSheet();

            LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                    new BroadcastReceiver(){
                        @Override
                        public void onReceive(Context context, Intent intent){

                            String latitude = intent.getStringExtra(GetLocationService.EXTRA_LATITUDE);
                            String longitude = intent.getStringExtra(GetLocationService.EXTRA_LONGITUDE);

                            if (latitude != null && longitude != null) {





                                location_list.add(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));

                           //   Toast.makeText(getContext(),latitude+" "+longitude,Toast.LENGTH_SHORT).show();
                                /*mylocation = locnow;*/

                                double accuracy = mylocation.getAccuracy();

                                boolean hasAccuracy = mylocation.hasAccuracy();

                                Log.i(TAG, "accuracy" + ">>" + accuracy + ">" + hasAccuracy);

                                int speed = (int) mylocation.getSpeed();

                                boolean hasSpeed = mylocation.hasSpeed();

                                Log.i(TAG, "speed" + ">>" + speed + ">" + hasSpeed);

                                double bearing = mylocation.getBearing();

                                boolean hasBearing = mylocation.hasBearing();

                                Log.i(TAG, "bearing" + ">>" + bearing + ">" + hasBearing);


                                if (location_list.size() > 1) {
                                    //--------------------------------

                                    if (hasAccuracy) {

                                        PolylineOptions options = new PolylineOptions().width(5).color(Color.CYAN).geodesic(true);
                                        for (int z = 0; z < location_list.size() - 1; z++) {
                                            LatLng point = location_list.get(z);
                                            options.add(point);
                                        }
                                        Polyline cyanPolyline = googleMap.addPolyline(options);

                                    }


                                    //mymap.addMarker(new MarkerOptions().position(location_list.get(location_list.size()-2)));
                                    animateMarker(marker, location_list.get(location_list.size() - 1), false);
                                    //marker.setPosition(location_list.get(location_list.size() - 1));

                                    float cbearing = (float)bearingBetweenLocations(location_list.get(location_list.size() - 2), location_list.get(location_list.size() - 1));
                                    Log.i("cbearing", ">>" + cbearing);

                                    if (cbearing > 20) {
                                        //marker.setRotation((float) bearing);
                                      rotateMarker(marker, cbearing);
                                    }


                                    marker.setAnchor(0.5f, 0.5f);

                                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(location_list.get(location_list.size() - 1)));
                                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                            .target(googleMap.getCameraPosition().target)
                                            .zoom(15.5f)
                                            .build()));

                                }
                            }
                        }
                    }, new IntentFilter(
                            GetLocationService.ACTION_LOCATION_BROADCAST)
            );
        }

    }

    private void sendmessage(String token, String msg){
        String url = "https://fcm.googleapis.com/fcm/send";

        JSONObject requestBody = new JSONObject();
        JSONObject dataBody = new JSONObject();
try {
    dataBody.put("type","BtoP");
    dataBody.put("Mymessage","Bus is near your pickup point");
    requestBody.put("to",token);
    requestBody.put("data",dataBody);

}catch (Exception e){

}
new SendMessage("sendmsg",requestBody,url,getContext(),Request.Method.POST,this).makehttprequest();

    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getContext()).registerReceiver((georeceiver),
                new IntentFilter("SEND_DATA_TO_LOCATION"));
        LocalBroadcastManager.getInstance(getContext()).registerReceiver((msgreceiver),
                new IntentFilter(Intent_Action));
    }

    private void navigatetolatlng(String add){
        Uri navigationIntentUri = Uri.parse("google.navigation:q="+add);//creating intent with latlng
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }
    private boolean isMarkerRotating = false;
    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2){

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }
    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }
    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 1000;
        final Interpolator interpolator = new LinearInterpolator();
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;

                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));
                //marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_top));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible(false);
                    } else {
                        marker.setVisible(true);
                    }
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        checkPermission();
    }
    public void checkPermission(){

        SharedPreferences permissionStatus = getActivity().getSharedPreferences("permissionStatus", MODE_PRIVATE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Show Information about why you need the permission
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
                builder.setTitle("Need Location Permission");
                builder.setMessage("This app needs location permission to work.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }else if (permissionStatus.getBoolean(android.Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                android.support.v7.app.AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Need Location Permission");
                builder.setMessage("This app needs location permission to work.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();

                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getContext(), "Go to Permissions to Grant Location", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, true);
            editor.commit();

        } else {
            //You already have the permission, just go ahead.
            initializeMapAndLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                initializeMapAndLocation();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs location permission to work");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                initializeMapAndLocation();
            }
        }
    }

    private void initializeMapAndLocation(){

        if (!ManagePermission.checkGPSAndNetwork(getContext())) {
            enableLocation();
            return;
        }

        if (locationPermission.checkPlayServices()) {
            //initialize map



                //initialize location service
              FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
                if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
            }


    }
    private void enableLocation(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("Location is not enabled");
        dialog.setPositiveButton("Open SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getContext().startActivity(myIntent);
                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
            }
        });
        dialog.show();
    }


    private void initializeBottomSheet(){
        // init the bottom sheet behavior
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet);

        // set callback for changes
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // Called every time when the bottom sheet changes its state.
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // Called when the bottom sheet is being dragged
                if (isAdded()) {
                    transitionBottomSheetBackgroundColor(slideOffset);
                    animatearrow(slideOffset);
                }
            }
        });
    }

    private void animatearrow(float slideOffset) {
        arrow_left.setRotation(slideOffset* -180);
        arrow_right.setRotation(slideOffset*180);
    }


    private void transitionBottomSheetBackgroundColor(float slideOffset){
        int colorFrom = getResources().getColor(android.R.color.black);
        int colorTo = getResources().getColor(android.R.color.darker_gray
        );
        bottom_sheet.setBackgroundColor(interpolateColor(slideOffset,
                colorFrom, colorTo));
    }


    private int interpolateColor(float fraction, int startValue, int endValue) {
        int startA = (startValue >> 24) & 0xff;
        int startR = (startValue >> 16) & 0xff;
        int startG = (startValue >> 8) & 0xff;
        int startB = startValue & 0xff;
        int endA = (endValue >> 24) & 0xff;
        int endR = (endValue >> 16) & 0xff;
        int endG = (endValue >> 8) & 0xff;
        int endB = endValue & 0xff;
        return ((startA + (int) (fraction * (endA - startA))) << 24) |
                ((startR + (int) (fraction * (endR - startR))) << 16) |
                ((startG + (int) (fraction * (endG - startG))) << 8) |
                ((startB + (int) (fraction * (endB - startB))));
    }

    ArrayList<String> parents_id = new ArrayList();

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {
        if (task.isSuccessful()) {
            int i = task.getResult().size();
                if(task.isSuccessful()){
                    for(QueryDocumentSnapshot snapshot1:task.getResult()){
                        Map map=snapshot1.getData();
                        WardinfoModel   wardinfoModel=new WardinfoModel();
                        wardinfoModel.setId(snapshot1.getId());
                        wardinfoModel.setAge(""+map.get("Age"));
                        wardinfoModel.setName(""+map.get("Ward's_Name"));
                        wardinfoModel.setClas(""+map.get("clas"));
                        wardinfoModel.setGender(""+map.get("gender"));
                        wardinfoModel.setImage(""+map.get("img_url"));
                        wardinfoModel.setG_name(""+map.get("Guardian's_Name"));
                        wardinfoModel.setG_phno(""+map.get("Guardian_phno"));
                        wardinfoModel.setToken(""+map.get("parents_token"));
                        arrayList.add(wardinfoModel);

                    }
                    if(arrayList.size()!=0){
                        wardListAdapter = new WardListAdapter(getContext(), arrayList,pickuptype);
                        wardListAdapter.OnBoardClick(this);
                        recycler_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recycler_list.setAdapter(wardListAdapter);
                    }
                    progressDialog.dismiss();
                }


        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        this.googleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        googleMap.clear();
        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setBuildingsEnabled(true);
//        googleMap.setIndoorEnabled(true);
//        googleMap.setTrafficEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        //  googleMap.addMarker(new MarkerOptions());
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

            setMyCurrentLocation(googleMap, mylocation);
            // makevollyrequest(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
          //  googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(m, 14));


    }
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void DiracttoLocation(){
String url=null;
        GPSTracker gpsTracker = new GPSTracker(getContext());
//        String lat= String.valueOf(gpsTracker.getLatitude());
//        String lng= String.valueOf(gpsTracker.getLongitude());
        String lat= String.valueOf(mylocation.getLatitude());
        String lng=String.valueOf(mylocation.getLongitude());

       Address.replace(" ", "+");

        try {
    url="https://maps.googleapis.com/maps/api/directions/json?" +
            "mode=driving&"
            + "transit_routing_preference=less_driving&"
            + "origin="+lat+","+lng+ "&"
            + "destination=" + Address + "&"
            + "key=" + getResources().getString(R.string.map_api_key);

    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
            url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, response + "");
                    try {
                        JSONArray jsonArray = response.getJSONArray("routes");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject route = jsonArray.getJSONObject(i);
                            JSONObject poly = route.getJSONObject("overview_polyline");
                            String polyline = poly.getString("points");
                            polyLineList = decodePoly(polyline);
                            Log.d(TAG, polyLineList + "");
                        }

                        //Adjusting bounds
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (LatLng latLng : polyLineList){
                            builder.include(latLng);
                        }
                        LatLngBounds bounds = builder.build();
                        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
                        googleMap.animateCamera(mCameraUpdate);

                    PolylineOptions    polylineOptions = new PolylineOptions();
                        //polylineOptions.color(Color.GRAY);
                        polylineOptions.color(Color.CYAN);
                        polylineOptions.width(5);
                        polylineOptions.startCap(new SquareCap());
                        polylineOptions.endCap(new SquareCap());
                        polylineOptions.jointType(ROUND);
                        polylineOptions.addAll(polyLineList);
                    Polyline    greyPolyLine = googleMap.addPolyline(polylineOptions);

                        PolylineOptions       blackPolylineOptions = new PolylineOptions();
                        blackPolylineOptions.width(5);
                        blackPolylineOptions.color(Color.GREEN);
                        //blackPolylineOptions.color(Color.BLACK);
                        blackPolylineOptions.startCap(new SquareCap());
                        blackPolylineOptions.endCap(new SquareCap());
                        blackPolylineOptions.jointType(ROUND);
                   Polyline     blackPolyline = googleMap.addPolyline(blackPolylineOptions);

                        googleMap.addMarker(new MarkerOptions()
                                .position(polyLineList.get(polyLineList.size() - 1)).title(count+","+Address));

                        ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
                        polylineAnimator.setDuration(3000);
                        polylineAnimator.setInterpolator(new LinearInterpolator());
                        polylineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                List<LatLng> points = greyPolyLine.getPoints();
                                int percentValue = (int) valueAnimator.getAnimatedValue();
                                int size = points.size();
                                int newPoints = (int) (size * (percentValue / 100.0f));
                                List<LatLng> p = points.subList(0, newPoints);
                                blackPolyline.setPoints(p);
                            }
                        });
                        polylineAnimator.start();

                    } catch (Exception e) {
                        Toast.makeText(getContext(),""+e+"756",Toast.LENGTH_LONG).show();
                    }

                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
Toast.makeText(getContext(),""+error+"763",Toast.LENGTH_LONG).show();
        }
    });
    RequestQueue requestQueue = Volley.newRequestQueue(getContext());
    requestQueue.add(jsonObjectRequest);
} catch (Exception e) {
    Toast.makeText(getContext(),""+e+"769",Toast.LENGTH_LONG).show();
}
    }


    private void setMyCurrentLocation(GoogleMap map, Location location) {
        if (map == null || location == null) {
            Toast.makeText(getContext(), "Problem occured. Failed to show current location", Toast.LENGTH_SHORT).show();
            return;
        } else {
            GPSTracker gpsTracker = new GPSTracker(getContext());


            if (gpsTracker.canGetLocation() == false){

                gpsTracker.showSettingsAlert();

            } else {

                myLatLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                marker = map.addMarker(new MarkerOptions()
                        .position(myLatLng)
                        .flat(false)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.yellow_bus)));


                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLatLng, 16.5f));
                getContext().startService(new Intent(getContext(), GetLocationService.class));
                DiracttoLocation();
            }
        }
    }
    @Override
    public void onSuccess(Location location) {
        this.mylocation = location;
        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map_view);
        if (mapFrag != null) {
            mapFrag.getMapAsync(this);
        }
    }


    @Override
    public void onLocationChanged(Location location){
        if (location != null) {
            mylocation=location;
            myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(myLatLng, 16.5f);
            googleMap.animateCamera(cameraUpdate);
            locMan.removeUpdates(this);

        }

        }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getContext(), provider + " has Enable", Toast.LENGTH_LONG).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getContext(), provider + " has Disable", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public String onResponse(String response, String type){
        try {
            JSONObject res = new JSONObject(response);
            int successCode = res.getInt("success");
            if (successCode == 1) {
                Toast.makeText(getContext(), "Request Send", Toast.LENGTH_SHORT).show();
                //  saverequesttime();

            }else if(successCode==0){
                Toast.makeText(getContext(), "Can't Send Request,try other service Center", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), "Request send But error in response", Toast.LENGTH_SHORT).show();
        }finally {
            progressDialog.dismiss();
        }
        return null;
    }

    @Override
    public VolleyError onFailure(VolleyError error) {
        Toast.makeText(getContext(),""+error,Toast.LENGTH_LONG).show();
        return null;
    }

    @Override
    public void onBoardClick(String childId, String token,String clas) {
     savedatatofirebase(childId,"intime",clas);
      sendmessagetoparents(token);
    }

    private void savedatatofirebase(String childId,String time,String clas) {
        String a;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy:MM:dd");
        SimpleDateFormat timeformater = new SimpleDateFormat("HH:mm:ss");

        Date date = new Date();
        Date d2=new Date() ;
        String t=timeformater.format(d2);
        String d=formatter.format(date);
        Map<String ,String>map=new HashMap<>();
        map.put("student_id",childId);
        map.put("date",d);
        map.put("time",t);
        map.put("clas",clas);

     FirebaseFirestore.getInstance().collection("PickAndDropReport")
             .document(d).collection(time).add(map).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
         @Override
         public void onComplete(@NonNull Task<DocumentReference> task) {

         }
     });

    }

    private void sendmessagetoparents(String token){
        progressDialog.show();
        String url = "https://fcm.googleapis.com/fcm/send";
     String s;
        JSONObject requestBody = new JSONObject();
        JSONObject dataBody = new JSONObject();
        if(pickuptype.equalsIgnoreCase("pickfromhome")){
            s="Picked From Home";
        }else {
            s="Droped to Home";
        }
        try {
            dataBody.put("type","BtoP");
            dataBody.put("pickup",s);
            requestBody.put("to",token);
            requestBody.put("data",dataBody);

        }catch (Exception e){

        }
        new SendMessage("sendmsg",requestBody,url,getContext(),Request.Method.POST,this).makehttprequest();
    }

    @Override
    public void onDeboardClick(String childId,String token){
        savedatatofirebase(childId,"outTime","");
        sendmessagetoparents(token);

    }
}




