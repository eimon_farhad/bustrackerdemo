package com.innovious.eimon.Bustracker.units;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;


public class SendMessage{
String type,url;
    Context context;
    int method;
    MessageLisner mListener;
    JSONObject requestbody;
    JSONObject databody;


    public SendMessage( String type, JSONObject requestbody,String url, Context context, int method, MessageLisner mListener) {
        this.type = type;
this.requestbody=requestbody;
        this.context = context;
        this.method = method;
        this.mListener = mListener;
        this.url=url;
    }

    public  void  makehttprequest(){

        if(isNetworkConnected()){

    StringRequest request=new StringRequest(method, url,
            response -> {

mListener.onResponse(response,type);
    },
            error -> {
mListener.onFailure(error);
    }){

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "key=AAAAm2j0IaM:APA91bHVfNEiErNOkh2vVXz0dwY7qXykekHFAVIBTh36Tl2jQByUoG-deqSs1ef13vMPv2OaRcVFG3Yj_X9vFHZmNXj2Cmhty2FISyc3mY5nRMtIGmFn7zG5pf9ZRo4Wv6Owj9gPv5VA");
            return headers;
        }

        @Override
        public byte[] getBody() throws AuthFailureError {

            String requestString = requestbody.toString();
            return requestString.getBytes();

        }



    };
    VolleySingleton.getInstance(context).addToRequestQueue(request);

}

    }




    public interface MessageLisner {
        String onResponse(String response, String type);
        VolleyError onFailure(VolleyError error);

    }
    public  boolean isNetworkConnected() {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
