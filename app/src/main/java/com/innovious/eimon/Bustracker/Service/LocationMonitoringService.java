package com.innovious.eimon.Bustracker.Service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.innovious.eimon.Bustracker.Constants;
import com.innovious.eimon.Bustracker.MyApplication;
import com.innovious.eimon.Bustracker.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class LocationMonitoringService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {


    public static final String ACTION_LOCATION_BROADCAST = LocationMonitoringService.class.getName() + "LocationBroadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    private static final String TAG = LocationMonitoringService.class.getSimpleName();
    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();
    Socket socket;
   ArrayList<LatLng> location_list ;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        mLocationRequest.setInterval(Constants.LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(Constants.FASTEST_LOCATION_INTERVAL);


        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes

location_list=new ArrayList<>();
        mLocationRequest.setPriority(priority);
        mLocationClient.connect();
        MyApplication app = (MyApplication) getApplication();

        socket = app.getSocket();

        socket.on(Socket.EVENT_CONNECT, onConnect);

        //socket.on("setlatlong", sendlatlong);

        socket.connect();

createNotificationChannel();
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentText("SendingLocation")
                .setSmallIcon(R.drawable.ic_cab)
                .build();


startForeground(1,notification);
        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        socket.close();
    }
    public static final String CHANNEL_ID = "ForegroundServiceChannel";

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public  Emitter.Listener onConnect = new Emitter.Listener(){
        @Override
        public void call(Object... args) {

            try {
                Log.i("joined", "socket connected");

                JSONObject obj = new JSONObject();

                /*obj.put("name", "trackme");
                obj.put("room", "9474094290");*/

                obj.put("name", "Bus");
                obj.put("room", "21");

                socket.emit("join", obj);

                Log.i("joined", "socket connected");

            } catch (JSONException je) {
                Log.i("joined", ""+je);

                je.printStackTrace();
            }

        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /*
     * LOCATION CALLBACKS
     */
    @Override
    public void onConnected(Bundle dataBundle){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        Log.d(TAG, "Connected to Google API");
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended");
    }


    //to get the location change
    @Override
    public void onLocationChanged(Location location){
        Log.d(TAG, "Location changed");


        if (location != null) {
            Log.d(TAG, "== location != null");

            //Send result to activities
            shareLocation(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
        }

    }

    private void shareLocation(String lat, String lng){

        Log.d(TAG, "Sending info...");
        location_list.add(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)));

        if (location_list.size() > 1) {

            //-----Emit Data to Socket-------

            JSONObject obj = new JSONObject();

            try {
                obj.put("lat", location_list.get(location_list.size() - 1).latitude);

                obj.put("lng", location_list.get(location_list.size() - 1).longitude);

            } catch (JSONException e) {
                Log.i("emit", ""+e);

            }
            senddatatoactivity(location_list.get(location_list.size() - 1).latitude,
                    location_list.get(location_list.size() - 1).longitude);
            socket.emit("sendlatlong", obj);

            Log.i("emit", "success");

        }

    }

    private void senddatatoactivity(double message,double type) {
        Intent intent = new Intent("MyIntent");
        // You can also include some extra data.
        intent.putExtra("Status", message);
        intent.putExtra("type",type);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
        Log.d(TAG, "Failed to connect to Google API");
    }


}