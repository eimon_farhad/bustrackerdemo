package com.innovious.eimon.Bustracker.BUS.Fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.innovious.eimon.Bustracker.LatlngModel;
import com.innovious.eimon.Bustracker.R;

import java.util.ArrayList;

public class DropFragment extends Fragment implements OnCompleteListener<QuerySnapshot> {
Button navigate;
FirebaseFirestore firestore;
ArrayList<LatlngModel>arrayList;
ProgressDialog progressDialog;
    CollectionReference reference;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dropfragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initview(view);
        initobj();
        reference.orderBy("pickuppoint", Query.Direction.DESCENDING).get().addOnCompleteListener(this);

    }

    private void initobj() {
        arrayList=new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");
        firestore = FirebaseFirestore.getInstance();
        reference = firestore.collection("Bus")
                .document("PickAndDropPoint")
                .collection("LatLng");
    }

    private void initview(View view) {
        navigate=view.findViewById(R.id.navigate);
    }

    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {

    }
}
