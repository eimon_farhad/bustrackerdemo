package com.innovious.eimon.Bustracker.BUS.Fragment;

import android.Manifest;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.innovious.eimon.Bustracker.LatlngModel;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.Service.GeofenceTrasitionService;

import java.util.ArrayList;
import java.util.Map;

public class PickupFragment extends Fragment implements View.OnClickListener, OnCompleteListener<QuerySnapshot> {
    Button navigate, student_list;
    TextView txt_pickuptype;
    FirebaseFirestore firestore;
    CollectionReference reference;
    ArrayList<LatlngModel> arrayList;
    LatLng latLng;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int count = 0;
    ProgressDialog progressDialog;
    private static final long GEO_DURATION = 60 * 60 * 1000;
    private static final String GEOFENCE_REQ_ID = "My Geofence";
    GeofencingClient geofencingClient;
    String school_address = " Street Number 358, DG Block(Newtown), Action Area I, Newtown, Kolkata, West Bengal 700156";
    static String preferenceName = "MyCounterPreference";
String pickuptype;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nextpickupfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initview(view);
        initobj();
        savedInstanceState=getArguments();
        if(savedInstanceState!=null){
            pickuptype=savedInstanceState.getString("pickuptype");
        }
        if (sharedPreferences.contains("mcount")){
            count = sharedPreferences.getInt("mycount", 0);
        }
 if(pickuptype.equalsIgnoreCase("pickfromhome")) {
   txt_pickuptype.setText("Pick From Home");
 }else if(pickuptype.equalsIgnoreCase("droptohome")){
     txt_pickuptype.setText("Drop To Home");
 }
 reference.orderBy("pickuppoint").get().addOnCompleteListener(this);
        progressDialog.show();
    }

    private void initobj(){
        student_list.setOnClickListener(this);
        navigate.setOnClickListener(this);
        arrayList = new ArrayList<>();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");
        firestore = FirebaseFirestore.getInstance();
        geofencingClient = LocationServices.getGeofencingClient(getActivity());

        sharedPreferences = getContext().getSharedPreferences(preferenceName, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        reference = firestore.collection("Bus")
                .document("PickAndDropPoint")
                .collection("LatLng");

    }

    private void initview(View view) {
        navigate = view.findViewById(R.id.navigate);
        student_list = view.findViewById(R.id.student_list);
        txt_pickuptype=view.findViewById(R.id.txt_pickuptype);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navigate:
                navigateto();
                break;
            case R.id.student_list:

                break;
        }
    }

    private void replacefrag(double slat, double slng, String address, int count, String pickuptype) {

        createGeofence(slat, slng);
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", slat);
        bundle.putDouble("lng", slng);
        bundle.putString("address", address);
        bundle.putString("count", String.valueOf(count));
      bundle.putString("pickuptype",pickuptype);

        NextPickUpMapFragment nextPickUpMapFragment = new NextPickUpMapFragment();
        nextPickUpMapFragment.setArguments(bundle);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        String backStateName = nextPickUpMapFragment.getClass().getName();
        boolean fragmentPopped = getFragmentManager().popBackStackImmediate(backStateName, 0);
        if (!fragmentPopped) {
            ft.replace(R.id.bus_fragment_container, nextPickUpMapFragment).addToBackStack(backStateName).commit();
        }
    }

    private void creategiofence(double slat, double slng) {
        Geofence geofence = createGeofence(slat, slng);
        if (!geofence.equals(null)) {
            GeofencingRequest request = createGeoRequest(geofence);
            if (!request.equals(null)) {
                addgeofence(request);
            }
        }

    }

    private void addgeofence(GeofencingRequest request) {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        geofencingClient.addGeofences(request, createGeofencePendingIntent())
                .addOnSuccessListener(v->{
                    Toast.makeText(getContext(),"Successfull added ",Toast.LENGTH_SHORT).show();

                }).addOnFailureListener(f->{
            Toast.makeText(getContext(),""+f,Toast.LENGTH_SHORT).show();

        });

    }
    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 1;

    private PendingIntent createGeofencePendingIntent(){
        if ( geoFencePendingIntent != null)
            return geoFencePendingIntent;

        Intent intent = new Intent( getContext(), GeofenceTrasitionService.class);
        return PendingIntent.getService(
                getContext(), GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    private GeofencingRequest createGeoRequest(Geofence geofence) {
        return new GeofencingRequest.Builder()
                .setInitialTrigger( GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofence( geofence)
                .build();
    }

    private Geofence createGeofence(double slat,double slng){

        try {
            return new Geofence.Builder()
                    .setRequestId(GEOFENCE_REQ_ID)
                    .setCircularRegion(slat,slng,500f)
                    .setExpirationDuration( GEO_DURATION)
                    .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER
                            | Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build();

        }catch (Exception e){
            Log.i("ABCd",""+e);
        }
        return null;
    }

    private void navigateto(){
        if(count!=0){
            count = sharedPreferences.getInt("mycount", 0);
        }
        try{
            int size=arrayList.size();
            //  int mycount=arrayList.get(count).getCount();
if(count==size){
    replacefrag(Double.parseDouble("22.588511"), Double.parseDouble("88.450218"),
            school_address,0, pickuptype);

    editor.clear().commit();
    Toast.makeText(getContext(),"Go back to School",Toast.LENGTH_LONG).show();
}else{
    if (count < size) {
        double Slat = arrayList.get(count).getLat();
        double Slng = arrayList.get(count).getLng();

      //creategiofence(Slat,Slng);
        replacefrag(Slat, Slng, arrayList.get(count).getAddress(),arrayList.get(count).getCount(),pickuptype);
        count++;
        savedatatoPreference(count);

    }else {
        Toast.makeText(getContext(), "Trip End", Toast.LENGTH_SHORT).show();
        editor.clear().commit();

    }
}
        }catch (Exception e){
            Toast.makeText(getContext(), ""+e, Toast.LENGTH_SHORT).show();
            editor.clear().commit();
        }
    }


    private void savedatatoPreference(int count){
editor.putInt("mycount",count).commit();
    }


    private void navigatetolatlng(String add){
        Uri navigationIntentUri = Uri.parse("google.navigation:q="+add);//creating intent with latlng
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, navigationIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }



    @Override
    public void onComplete(@NonNull Task<QuerySnapshot> task) {


        for (QueryDocumentSnapshot snapshot:task.getResult()){
            Map map=snapshot.getData();
            Log.i("pickuppoint",""+map.get("pickuppoint"));
            double lat=Double.parseDouble((String) map.get("lat"));
            double lng=Double.parseDouble((String) map.get("lng"));
            int pickcout= Integer.parseInt((String) map.get("pickuppoint"));
            String addess=(String)map.get("address");
            LatlngModel latlngModel=new LatlngModel();
            latlngModel.setLat(lat);
            latlngModel.setLng(lng);
            latlngModel.setCount(pickcout);
            latlngModel.setAddress(addess);
            arrayList.add(latlngModel);
        }
      progressDialog.dismiss();
    }
}
