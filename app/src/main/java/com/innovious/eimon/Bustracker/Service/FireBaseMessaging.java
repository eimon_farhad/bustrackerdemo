package com.innovious.eimon.Bustracker.Service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.innovious.eimon.Bustracker.R;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class FireBaseMessaging extends FirebaseMessagingService {
public  static String Intent_Action="com.innovious.eimon.Bustracker.Service.Intent_Action";

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){

        if(remoteMessage.getData().size()!=0){
            Map<String,String>map=remoteMessage.getData();
            String type=map.get("type");

            switch (type){
                case "BtoP":
                    if(map.containsKey("pickup")){
                    notifyuser(map.get("pickup"));
                    }else if(map.containsKey("Mymessage")){
                        notifyuser(map.get("Mymessage"));
                    }
                    break;
                case "PtoB":
       String usertoken,child_id;
       usertoken=map.get("fmc_token");
       child_id=map.get("childid");

                    senddatatoactivty(usertoken,child_id);
                    break;

            }

        }
    }

    private void senddatatoactivty(String usertoken, String child_id) {
        Intent intent=new Intent(Intent_Action);
        intent.putExtra("usertoken",usertoken);
        intent.putExtra("child_id",child_id);

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);



    }

    private void notifyuser(String msg) {
        int notificationId = new Random().nextInt(6000);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,"123")
                .setSmallIcon(R.drawable.ic_cab)
                .setContentTitle(msg)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);
        NotificationManager notificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        setupChannels("mymsg");
        notificationManager.notify(notificationId,builder.build());
    }

    private void setupChannels(String mes){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "mychannel";
            String description =mes;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("123", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
    }
}
