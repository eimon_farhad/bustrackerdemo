package com.innovious.eimon.Bustracker.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.innovious.eimon.Bustracker.Adapter.WardListAdapter;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.WardinfoModel;
import com.innovious.eimon.Bustracker.units.ListClickListener;
import com.innovious.eimon.Bustracker.units.SendMessage;
import com.innovious.eimon.Bustracker.units.UserPref;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class WardsListFragment extends Fragment implements OnCompleteListener , ListClickListener,SendMessage.MessageLisner {
    TextView add_wards;
    ImageView _img_back;
    RecyclerView wards_list;
    WardListAdapter wardListAdapter;
    ArrayList<WardinfoModel>arrayList;
    FirebaseAuth auth;
    FirebaseFirestore firestore;
    ProgressDialog progressDialog;
    String uid;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wardslistfragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        add_wards = view.findViewById(R.id.add_wards);
        _img_back=view.findViewById(R.id._img_back);
        wards_list=view.findViewById(R.id.wards_list);
    auth=FirebaseAuth.getInstance();
    arrayList=new ArrayList<>();
    progressDialog=new ProgressDialog(getContext());
    progressDialog.setMessage("Loading...");
    String ph_no=auth.getCurrentUser().getPhoneNumber();
  firestore=FirebaseFirestore.getInstance();
        progressDialog.show();
        firestore.collection("Parents")
                .whereEqualTo("Phone_number",ph_no)
                .get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                for(QueryDocumentSnapshot snapshot:task.getResult()){
                    uid=snapshot.getId();
                }
                if(uid!=null){
                    getchild(uid);
                }
                progressDialog.dismiss();
            }
        }).addOnFailureListener(e -> {
            progressDialog.dismiss();
            Toast.makeText(getContext(),""+e,Toast.LENGTH_LONG).show();
        });






        _img_back.setOnClickListener(v->{
            getActivity().finish();
        });
        add_wards.setOnClickListener(v -> {
            replacefragment(new AddWardsfragment());
        });
    }

    private void getchild(String uid) {
        progressDialog.show();
        firestore.collection("Son").whereEqualTo("parents_id", uid)
                .get().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                for (QueryDocumentSnapshot snapshot:task.getResult()){
                    Map map=snapshot.getData();
                    WardinfoModel   wardinfoModel=new WardinfoModel();
                    wardinfoModel.setId(snapshot.getId());
                    wardinfoModel.setAge(""+map.get("Age"));
                    wardinfoModel.setName(""+map.get("Ward's_Name"));
                    wardinfoModel.setClas(""+map.get("clas"));
                    wardinfoModel.setGender(""+map.get("gender"));
                    wardinfoModel.setImage(""+map.get("img_url"));
                    wardinfoModel.setG_name(""+map.get("Guardian's_Name"));
                    wardinfoModel.setG_phno(""+map.get("Guardian_phno"));
                    wardinfoModel.setToken(""+map.get("parents_token"));
                    arrayList.add(wardinfoModel);

                }
                if (arrayList.size()>=1){
                    wardListAdapter = new WardListAdapter(getContext(), arrayList,"droptohome");
                    wards_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    wards_list.setAdapter(wardListAdapter);

                }
                progressDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e){
                Log.i("log",e+"");
                progressDialog.dismiss();
            }
        });
    }


    public void replacefragment(Fragment fg) {
        String backStateName = fg.getClass().getName();
        boolean fragmentPopped = getChildFragmentManager().popBackStackImmediate(backStateName, 0);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (!fragmentPopped){
            ft.addToBackStack(backStateName);
            ft.replace(R.id.fragment_container, fg).commit();
        }
    }

    @Override
    public void onComplete(@NonNull Task task) {

    }

    @Override
    public void onBoardClick(String childId,String token) {

    }

    @Override
    public void onDeboardClick(String childId,String token) {
        String url = "https://fcm.googleapis.com/fcm/send";
        JSONObject object=new JSONObject();
        JSONObject request=new JSONObject();
        try {
            object.put("type","PtoB");
            object.put("childid",childId);
            object.put("fmc_token",new UserPref(getContext()).getToken());

            request.put("to",token);
            request.put("data",object);

        }catch (Exception e){

        }
        new SendMessage("PtoB",request,url,getContext(), Request.Method.POST,this).makehttprequest();
    }

    @Override
    public String onResponse(String response, String type) {

        try {
            JSONObject res = new JSONObject(response);
            int successCode = res.getInt("success");
            if (successCode == 1) {
                Toast.makeText(getContext(), "Request Send", Toast.LENGTH_SHORT).show();
                //  saverequesttime();

            }else if(successCode==0){
                Toast.makeText(getContext(), "Can't Send Request,try other service Center", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), "Request send But error in response", Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    @Override
    public VolleyError onFailure(VolleyError error) {
        Toast.makeText(getContext(), "Error Occured", Toast.LENGTH_SHORT).show();
        return null;
    }
}