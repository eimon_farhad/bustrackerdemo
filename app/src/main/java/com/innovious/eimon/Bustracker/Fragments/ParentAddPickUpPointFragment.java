package com.innovious.eimon.Bustracker.Fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.innovious.eimon.Bustracker.LocationPermission;
import com.innovious.eimon.Bustracker.R;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class ParentAddPickUpPointFragment extends Fragment implements   OnMapReadyCallback,
        OnSuccessListener<Location>, GoogleMap.OnMarkerClickListener {
    SupportMapFragment mapFragment;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location location;
    LocationPermission locationPermission;
    GoogleMap googleMap;
    LatLng pickuplatlng;
    RadioButton  radioButton;
    private int LOCATION_PERMISSION_CONSTANT = 200;
    private static final int REQUEST_PERMISSION_SETTING = 301;
TextView address;
Button btn_done;
ProgressDialog pd;
FirebaseFirestore firestore;
double lat,lng;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.parentsaddpickuppoint, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        address=view.findViewById(R.id.address);
        btn_done=view.findViewById(R.id.btn_done);

        firestore=FirebaseFirestore.getInstance();

        pd=new ProgressDialog(getContext());

        pd.setMessage("Laoding...");
        pd.setCanceledOnTouchOutside(false);
        btn_done.setOnClickListener(v->{
            replaceFragment();
//            googleMap.addMarker(new MarkerOptions()
//                    .position(pickuplatlng)
//                    .flat(true));

        });
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map_view);

      checkPermission();


    }

    private void getdata(){
        pd.show();
        if(googleMap!=null){
            googleMap.clear();

        }
     try {
         firestore.collection("Bus")
                 .document("PickAndDropPoint")
                 .collection("LatLng").get()
                 .addOnCompleteListener(task -> {
                     if(task.isComplete()){
                         for(QueryDocumentSnapshot snapshot: task.getResult()){
                             Map map=snapshot.getData();
                             double lat=Double.parseDouble((String) map.get("lat"));
                             double lng=Double.parseDouble((String) map.get("lng"));
                             String address=(String)map.get("address");

                             LatLng latLng=new LatLng(lat,lng);
                             googleMap.addMarker(new MarkerOptions()
                                     .position(latLng).title(address)
                                     .icon(BitmapDescriptorFactory.fromResource(R.drawable.maps_icon)).flat(false));
                         }
                         googleMap.setOnMarkerClickListener(this);
                     }
                     pd.dismiss();

                 }).addOnFailureListener(e -> {
             Toast.makeText(getContext(),""+e,Toast.LENGTH_SHORT).show();


             pd.dismiss();

         });
     }catch (Exception e){
         Toast.makeText(getContext(),""+e,Toast.LENGTH_SHORT).show();
     }

    }



    private void replaceFragment(){

        AddWardsfragment fg=new AddWardsfragment();
        Bundle bundle=new Bundle();
        bundle.putString("address",addresss);
        bundle.putDouble("lat",lat);
        bundle.putDouble("lng",lng);
        fg.setArguments(bundle);

        String backStateName = fg.getClass().getName();
        boolean fragmentPopped = getChildFragmentManager().popBackStackImmediate(backStateName, 0);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (!fragmentPopped) {
            ft.addToBackStack(backStateName);
            ft.replace(R.id.fragment_container, fg).commit();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                initializeMapAndLocation();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs location permission to work");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                initializeMapAndLocation();
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap){
this.googleMap=googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        googleMap.setMyLocationEnabled(true);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.setBuildingsEnabled(true);
//        googleMap.setIndoorEnabled(true);
//        googleMap.setTrafficEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        //  googleMap.addMarker(new MarkerOptions());
        if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        if (googleMap != null && location != null) {
            // setMyCurrentLocation(googleMap, location);
            // makevollyrequest(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
           setMyCurrentLocation(googleMap,location);
           // googleMap.setInfoWindowAdapter(new Popup);

        }

    }
LatLng latLng;
    @Override
    public void onSuccess(Location location){
this.location=location;
        mapFragment.getMapAsync(this);
        if (googleMap != null && location != null) {
            // setMyCurrentLocation(googleMap, location);
          //  googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
             setMyCurrentLocation(googleMap, location);

        }
    }
    private void setMyCurrentLocation(GoogleMap map, Location location) {
        if (map == null || location == null) {
            Toast.makeText(getContext(), "Problem occured. Failed to show current location", Toast.LENGTH_SHORT).show();
            return;
        }


        latLng = new LatLng(location.getLatitude(), location.getLongitude());


        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.5f));
        Geocoder geoCoder = new Geocoder(getContext(), Locale.getDefault());
//googleMap.addMarker(new MarkerOptions()
//                .position(latLng));


        //set map drag listener

    }



    private boolean checkPlayServices(){
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(getContext());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (gApi.isUserResolvableError(resultCode)) {
                gApi.getErrorDialog(getActivity(), resultCode, 100).show();
            } else {
                Toast.makeText(getContext(), "Google PlayService not available", Toast.LENGTH_LONG).show();
                //finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public void onResume() {
        super.onResume();
checkPermission();
        //getmycurrentLocation();
    }





    public void checkPermission(){

        SharedPreferences permissionStatus = getActivity().getSharedPreferences("permissionStatus", MODE_PRIVATE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Need Location Permission");
                builder.setMessage("This app needs location permission to work.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }else if (permissionStatus.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Need Location Permission");
                builder.setMessage("This app needs location permission to work.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        dialog.cancel();

                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getContext(), "Go to Permissions to Grant Location", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CALL_PHONE}, LOCATION_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, true);
            editor.commit();

        } else {
            //You already have the permission, just go ahead.
            initializeMapAndLocation();
            getdata();
        }
    }

    private void initializeMapAndLocation(){

        if (!checkGPSAndNetwork()) {
            enableLocation();
            return;
        }

        if (checkPlayServices()) {
            //initialize map

            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragment_map_view);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);

                //initialize location service
                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
                if (ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
            }
        } else {
            Toast.makeText(getContext(), "Unable to show map", Toast.LENGTH_SHORT).show();
        }

    }
    private boolean checkGPSAndNetwork(){

        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            try {
                locationMode = Settings.Secure.getInt(getContext().getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e){
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }
    private void enableLocation(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        dialog.setMessage("Location is not enabled");
        dialog.setPositiveButton("Open SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getContext().startActivity(myIntent);
                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
            }
        });
        dialog.show();
    }
    public String addresss;

    @Override
    public boolean onMarkerClick(Marker marker) {
        LatLng latLng=marker.getPosition();
        lat=latLng.latitude;
        lng=latLng.longitude;
        addresss=marker.getTitle();
        address.setText(addresss);
        return true;
    }
}
