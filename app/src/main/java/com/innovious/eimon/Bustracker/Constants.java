package com.innovious.eimon.Bustracker;

public class Constants {
    public final static String SOCKETURL= "http://tridab.se:6001";
    public static final int TAG_CODE_PERMISSION_LOCATION = 1;
    public static final int LOCATION_INTERVAL = 5000;
    public static final long FASTEST_LOCATION_INTERVAL = 2000;
}
