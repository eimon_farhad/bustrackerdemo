package com.innovious.eimon.Bustracker.units;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.innovious.eimon.Bustracker.LoginActivity;

import java.util.HashMap;
import java.util.Map;

public class FireBaseToken {
Context  context;

    private FireBaseToken() {
    }
    public static FireBaseToken getInstance(){
        return new FireBaseToken();
    };

    public void saveFirebaseToken(Context context){
        FirebaseApp.initializeApp(context);
        this.context=context;

        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if(task.isSuccessful()){
                String    token=task.getResult().getToken();
                    FirebaseMessaging.getInstance().subscribeToTopic("all");
                    new UserPref(context).saveFirebasetoken(token);
                    saveParentsDatatoFireBase(token);

                }
            }
        });

    }
    private void   saveParentsDatatoFireBase(String token){
        FirebaseAuth firebaseAuth=FirebaseAuth.getInstance();
        Map<String,String> map=new HashMap<>();
        map.put("FirebaseToken",token);
        map.put("Phone_number",firebaseAuth.getCurrentUser().getPhoneNumber());
        FirebaseFirestore.getInstance().collection("Parents").add(map).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task){

              //  Toast.makeText(context,"Token Added Successfully",Toast.LENGTH_LONG).show();
               // progressDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context,""+e,Toast.LENGTH_LONG).show();
               // progressDialog.dismiss();
            }
        });
    };
}
