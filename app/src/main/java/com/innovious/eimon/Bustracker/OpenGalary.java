package com.innovious.eimon.Bustracker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class OpenGalary {
   Context context;
   android.widget.ImageView ImageView;
    private int GALLERY = 1, CAMERA = 2;
    Fragment f;
    public OpenGalary(Context context, android.widget.ImageView imageView, Fragment f){
        this.context = context;
        ImageView = imageView;
        this.f=f;
    }

    public void requestMultiplePermissions() {

        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
        if (!addPermission(permissionsList, android.Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");

        if(!addPermission(permissionsList, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ_EXTERNAL_STORAGE");

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant  access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(context, message,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(((Activity)context),
                                        permissionsList.toArray(new String[permissionsList.size()]),
                                        100);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(((Activity)context), permissionsList.toArray(new String[permissionsList.size()]),
                    100);
            return;
        }else{
            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            f.startActivityForResult(galleryIntent, 1);
        }

    }
    public static void showMessageOKCancel(Context context, String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            return ActivityCompat.shouldShowRequestPermissionRationale(((Activity) context), permission);
        }
        return true;
    }
//    public void showPictureDialog() {
//        final Dialog dialog=new Dialog(context);
//        dialog.setContentView(R.layout.selectcamera);
//        android.widget.ImageView img_galary=dialog.findViewById(R.id.img_galary);
//        android.widget.ImageView img_camera=dialog.findViewById(R.id.img_camera);
//        img_camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                takePhotoFromCamera();
//                dialog.dismiss();
//            }
//        });
//        img_galary.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                choosePhotoFromGallary();
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//
//
//    }

    private void choosePhotoFromGallary(){
        
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        f.startActivityForResult(galleryIntent, GALLERY);



    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
       f.startActivityForResult(intent, CAMERA);
    }
}
