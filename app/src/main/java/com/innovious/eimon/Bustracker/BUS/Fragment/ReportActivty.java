package com.innovious.eimon.Bustracker.BUS.Fragment;

import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.innovious.eimon.Bustracker.R;
import com.innovious.eimon.Bustracker.units.Datepicker;

import java.util.ArrayList;
import java.util.Map;

public class ReportActivty extends AppCompatActivity implements Datepicker.ondateselectListener{
EditText edt_classs;
TextView ss,txt_date;
ImageView img;
Button btn_report;
RelativeLayout date;
RecyclerView recycler_view;
String d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_activty);
        edt_classs=findViewById(R.id.edt_classs);
        recycler_view=findViewById(R.id.recycler_view);
        ss=findViewById(R.id.ss);
        img=findViewById(R.id.img);
        btn_report=findViewById(R.id.btn_report);
        txt_date=findViewById(R.id.txt_date);
        date=findViewById(R.id.date);

        date.setOnClickListener(v->{
            DialogFragment dialogFragment=new Datepicker();
            ((Datepicker) dialogFragment).ondateset(this);
            dialogFragment.show(getSupportFragmentManager(), "Date Picker");

        });
        btn_report.setOnClickListener(v->{
            String s=edt_classs.getText().toString();
            generatereport(s,d);

        });






    }
ArrayList<InoutModel>idlist=new ArrayList<>();
    private void generatereport(String s, String d) {
        FirebaseFirestore.getInstance().collection("PickAndDropReport").document("d")
                .collection("outTime").whereEqualTo("clas",s).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isComplete()){
                    for(QueryDocumentSnapshot snapshot:task.getResult()){
                        Map map=snapshot.getData();
                        InoutModel model=new InoutModel();
                        model.setInTime(""+map.get("time"));
                    }
                }
            }
        });

        FirebaseFirestore.getInstance().collection("PickAndDropReport")
                .document(d).collection("intime")
                .whereEqualTo("clas",s).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isComplete()){
                    for(QueryDocumentSnapshot snapshot:task.getResult()){
                        Map map=snapshot.getData();
                        InoutModel model=new InoutModel();
                        model.setInTime(""+map.get("time"));
                        model.setChildId(""+map.get("student_id"));
                    }
                }
            }
        });

    }

    @Override
    public void ondateset(String date) {
        this.d=date;
txt_date.setText(View.VISIBLE);
ss.setVisibility(View.GONE);
img.setVisibility(View.GONE);
txt_date.setText(date);
    }
    class InoutModel{
        String inTime;
        String outTime;

        public String getInTime() {
            return inTime;
        }

        public void setInTime(String inTime) {
            this.inTime = inTime;
        }

        public String getOutTime() {
            return outTime;
        }

        public void setOutTime(String outTime) {
            this.outTime = outTime;
        }

        public String getChildId() {
            return childId;
        }

        public void setChildId(String childId) {
            this.childId = childId;
        }

        String childId;
    }
}
